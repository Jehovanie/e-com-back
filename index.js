import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import dotenv from "dotenv";
import mongoose from "mongoose";

import publicRoute from "./src/routes/public.js";
import authRoute from "./src/routes/auth.js"

dotenv.config();

const app = express();
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true }))
app.use(bodyParser.json({ limit: "30mb", extended: true }));
app.use(cors());

const PORT = process.env.PORT || 5000;

const CONNECTION_URL = `mongodb://localhost:27017/${process.env.DB_LOCALNAME}`;
// const CONNECTION_URL= "mongodb+srv://"+process.env.DB_USERNAME + ":" +process.env.DB_PASSWORD + "@atlascluster.ftplxht.mongodb.net/?retryWrites=true&w=majority"


app.get("/", (req, res) => {
    res.status(200).json({ "message": "ok" })
})

app.use("/", authRoute)
app.use("/api", publicRoute)


mongoose.connect(CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => app.listen(PORT, () => console.log(`Server running on http://localhost:${PORT}`)))
    .catch((error) => console.log("ERROR : " + error.message));
