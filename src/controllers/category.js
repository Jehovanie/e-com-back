
import categoriesModel from "../models/categories.js"
import productsModel from "../models/products.js"


export const getAllCategory = async (req, res) => {
    const { page } = req.query;
    const limit = 5;
    const skip = (parseInt(page) - 1) * limit;
    try {
        const total_category = await categoriesModel.countDocuments({});
        const catagories = await categoriesModel.find().sort({ _id: -1 }).limit(limit).skip(skip);

        const all_category = await Promise.all(catagories.map(async category => {

            const { _id: category_id } = category;

            const total_product = await productsModel.countDocuments({ category_id });
            const same_product = await productsModel.find({ category_id }).sort({ _id: -1 }).limit(10);

            return { ...category._doc, total_product, same_product }
        }))
        res.status(200).json({ "result": "get all category", "message": "ok", "data": { total_category, all_category } })
    } catch (error) {
        res.status(404).json({ "result": "get count category", "message": error.message, "data": 0 })
    }

}

export const createNewCategory = async (req, res) => {

    const category = req.body;
    if (!req.userId) {
        return res.status(401).json({ "result": "create new category", "message": "ko", "data": {} })
    }
    try {
        const newCatagory = new categoriesModel({ ...category })

        await newCatagory.save();

        res.status(201).json({ "result": "create new category", "message": "ok", "data": { newCatagory } })
    } catch (error) {
        res.status(409).json({ "result": "create new category", "message": error.message, "data": {} })
    }

}