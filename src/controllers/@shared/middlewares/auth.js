
import jwt from "jsonwebtoken";

const auth = (req, res, next) => {
    const token = req.headers.authorization?.split(" ")[1];
    if (token) {
        const decode_data = jwt.verify(token, process.env.PASSPHRASE);
        req.userId = decode_data._id;
    }
    next();
}

export default auth;