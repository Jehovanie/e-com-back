
import mongoose from "mongoose";
import productsModel from "../models/products.js"

export const getAllProduct = async (req, res) => {

    const { category_id, page } = req.params;



    try {
        if (!mongoose.Types.ObjectId.isValid(category_id) && !mongoose.Types.ObjectId.isValid(_id))
            throw new Error("category id not define")

        const limit = 5;
        const total_product = await productsModel.countDocuments({ category_id });
        const all_product = await productsModel.find({ category_id }).sort({ _id: -1 }).limit(limit).skip((parseInt(page) - 1) * limit);

        res.status(200).json({ "result": "get all product", "message": "ok", "data": { total_product, all_product } })
    } catch (error) {
        res.status(404).json({ "result": "get count product", "message": error.message, "data": 0 })
    }

}

export const createNewProduct = async (req, res) => {
    const { category_id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(category_id))
        return res.status(404).json({ "result": "get product", "message": "category id not define", "data": 0 })

    try {
        const product = req.body;
        const new_product = new productsModel({ ...product, category_id: category_id })
        await new_product.save();
        res.status(201).json({ "result": "create new product", "message": "ok", "data": { new_product } })
    } catch (error) {
        res.status(409).json({ "result": "create new product", "message": error.message, "data": {} })
    }

}

export const getOneProduct = async (req, res) => {

    const { category_id, product_id: _id } = req.params;

    try {

        if (!mongoose.Types.ObjectId.isValid(category_id) && !mongoose.Types.ObjectId.isValid(_id))
            throw new Error("category id or product id are not possible")

        const product = await productsModel.findOne({ _id, category_id });

        res.status(200).json({ "result": "one product", "message": "ok", "data": { product } })

    } catch (error) {
        res.status(409).json({ "result": "one product", "message": error.message, "data": {} })
    }

}

export const deleteProduct = async (req, res) => {

    const { category_id, product_id: _id } = req.params;

    try {
        if (!mongoose.Types.ObjectId.isValid(category_id) && !mongoose.Types.ObjectId.isValid(_id))
            throw new Error("category id or product id are not possible")

        await productsModel.findOneAndDelete({ _id, category_id });
        res.status(204)
    } catch (error) {
        res.status(409).json({ "result": "delete one product", "message": error.message, "data": {} })
    }

}

export const eventOnProduct = async (req, res) => {

    const { category_id, product_id: _id } = req.params;

    /** 
     * Action possible for one product:
     *      - on user add in your pannier, or user payd (state: { pedding, finished })
     * 
     */


}