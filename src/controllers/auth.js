
import usersModel from "../models/users.js"
import bcrypt from "bcryptjs"
import jwt from "jsonwebtoken"


export const signin = async (req, res) => {
    const { password, email } = req.body;
    try {
        const existing_user = await usersModel.findOne({ email });

        if (!existing_user)
            return res.status(400).json({ "result": "siging", "message": "ko" })

        const is_password_correct = bcrypt.compare(password, existing_user.password);

        if (!is_password_correct)
            return res.status(400).json({ "result": "siging", "message": "ko" });

        const token = jwt.sign({ "_id": existing_user._id, "name": existing_user.name, "email": existing_user.email }, process.env.PASSPHRASE, { expiresIn: "15m" });

        res.status(200).json({ "result": "signin", "message": "ok", data: { "user": existing_user, token } })
    } catch (error) {
        console.log(error)
        res.status(500).json({ "result": "signin", "message": "ko", data: {} })
    }

}

export const signup = async (req, res) => {

    const { password, confirm_password } = req.body;

    if (password !== confirm_password) {
        return res.status(400).json({ "result": "signup", "message": "ko" })
    }

    try {
        const { name, email, address, city, state, country, code_postal, phone } = req.body;


        if (await usersModel.findOne({ email })) {
            return res.status(400).json({ "result": "signup", "message": "ko" })
        }

        const hash_password = await bcrypt.hash(password, 12);

        const user = new usersModel({ name, email, address, city, state, country, code_postal, phone, "password": hash_password })
        const user_saved = await user.save();

        const token = jwt.sign({ "_id": user_saved._id, name, email }, process.env.PASSPHRASE, { expiresIn: "15m" });
        res.status(201).json({ "result": "signup", "message": "ok", data: { "user": user_saved, token } })
    } catch (error) {
        console.log(error)
        res.status(500).json({ "result": "signup", "message": "ko", data: {} })
    }


}