import express from "express";
import { createNewCategory, getAllCategory } from "../controllers/category.js";
import { createNewProduct, deleteProduct, getAllProduct, getOneProduct, eventOnProduct } from "../controllers/product.js";
import auth from "../controllers/@shared/middlewares/auth.js";

const router = express.Router();

router.get("/category", getAllCategory);
router.post("/category", auth, createNewCategory);

router.get("/category/:category_id/products", getAllProduct)
router.get("/category/:category_id/products/:product_id", getOneProduct)
router.post("/category/:category_id/products", auth, createNewProduct)
router.delete("/category/:category_id/products/:product_id", auth, deleteProduct)

router.patch("/category/:category_id/products/:product_id", eventOnProduct)
// router.get("/category/:id", getOneCategory);
// router.patch("/:id", updateCategory);
// router.delete("/:id", deleteOneCategory);

export default router;