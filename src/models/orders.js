import mongoose from "mongoose";

const ordersSchema = mongoose.Schema({
    
    user_id: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        required: true,
    },
    total_price: {
        type: Number,
        required: true,
    },
    created_at: {
        type: String,
        default: new Date().toString(),
        required: true
    },
    updated_at: {
        type: String,
        default: new Date().toString(),
        required: true
    }

})

export default mongoose.model("order", ordersSchema)