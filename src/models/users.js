import mongoose from "mongoose";

const userSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    address: {
        type: String,
        required: true,
    },
    city: {
        type: String,
        required: true,
    },
    state: {
        type: String,
        required: true,
    },
    country: {
        type: String,
        required: true,
    },
    code_postal: {
        type: String,
        required: true,
    },
    phone: {
        type: Number,
        required: true
    },
    created_at: {
        type: String,
        default: new Date().toString(),
        required: true
    },
    updated_at: {
        type: String,
        default: new Date().toString(),
        required: true
    }
});

export default mongoose.model("users", userSchema);