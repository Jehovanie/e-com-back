import mongoose from "mongoose";

const ordersItemSchema = mongoose.Schema({
    
    user_id: {
        type: String,
        required: true,
    },
    product_id: {
        type: String,
        required: true,
    },
    quantity: {
        type: Number,
        required: true,
    },
    price: {
        type: Number,
        required: true,
    },
    created_at: {
        type: String,
        default: new Date().toString(),
        required: true
    },
    updated_at: {
        type: String,
        default: new Date().toString(),
        required: true
    }

})

export default mongoose.model("orderItem", ordersItemSchema)