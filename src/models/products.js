import mongoose from "mongoose"

const productsSchema = mongoose.Schema({

    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    category_id: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: true,
    },
    quantity: {
        type: Number,
        required: true,
        default: 1
    },
    created_at: {
        type: String,
        default: new Date().toString(),
        required: true
    },
    updated_at: {
        type: String,
        default: new Date().toString(),
        required: true
    }
});

export default mongoose.model("product", productsSchema);