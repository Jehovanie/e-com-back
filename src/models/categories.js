import mongoose from "mongoose";

const categoriesSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    created_at: {
        type: String,
        default: new Date().toString(),
        required: true
    },
    updated_at: {
        type: String,
        default: new Date().toString(),
        required: true
    }
});

export default mongoose.model("categories", categoriesSchema)